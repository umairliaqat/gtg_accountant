class CreatePettyCashes < ActiveRecord::Migration[6.0]
  def change
    create_table :petty_cashes do |t|
      t.float :amount
      t.references :finance_transaction, null: false, foreign_key: true
      t.references :finance_account, null: false, foreign_key: true

      t.timestamps
    end
  end
end