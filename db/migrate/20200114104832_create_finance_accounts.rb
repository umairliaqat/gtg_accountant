
class CreateFinanceAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :finance_accounts do |t|
      t.string :title
      t.string :type
      t.float :opening_balance
      t.float :credit_limit
      t.float :credit_amount
      t.text :description

      t.references :customer, foreign_key: true
      t.references :supplier, foreign_key: true
      t.references :bank, null: false, foreign_key: true

      t.timestamps
    end
  end
end

