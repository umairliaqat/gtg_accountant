
class CreateBanks < ActiveRecord::Migration[6.0]
  def change
    create_table :banks do |t|
      t.string :name
      t.string :payment_category
      t.string :default_payment_method
      t.string :branch_name
      t.string :branch_code
      t.string :bank_account_number

      t.timestamps
    end
  end
end