class FinanceTransaction < ApplicationRecord
  belongs_to :bank
  belongs_to :customer
  belongs_to :supplier, optional: true

  validates :date, :payee, :description, :selection, :ref_no, :spent_amount, :received, presence:true
  #validates :spent_amount, :received, :numericality  => { greater_than: 0 }
end


