class Supplier < ApplicationRecord
  has_one :finance_account
  has_many :banks
  has_many :finance_transactions

  validates :ntn, :cnic, :telephone, :mobile, uniqueness: true, presence: true

end

