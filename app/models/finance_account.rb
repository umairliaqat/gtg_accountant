
class FinanceAccount < ApplicationRecord
  belongs_to :bank
  belongs_to :customer
  belongs_to :supplier,  optional: true
  has_many :liabilities, dependent: :destroy
  has_many :assets, dependent: :destroy
  has_many :expenses, dependent: :destroy
  has_many :equities, dependent: :destroy
  has_many :incomes, dependent: :destroy
  #
  #TO DO
  #these validates have some problem so resolve it
  #validates :opening_balance, :credit_limit, :credit_amount, :description, presence: true
  #validates :opening_balance, :credit_limit, :credit_amount, :numericality  => { greater_than: 0 }

end
