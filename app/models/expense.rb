class Expense < ApplicationRecord
  belongs_to :finance_account

  validates :name, uniqueness: true

end
