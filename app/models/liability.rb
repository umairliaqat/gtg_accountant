class Liability < ApplicationRecord

  belongs_to :finance_account

  validates :name, :finance_account_id, uniqueness: true, presence: true
end
