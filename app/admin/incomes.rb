
ActiveAdmin.register Income do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  #
  form do |f|
    f.semantic_errors *f.object.errors.keys
    inputs "Details" do
      input :name

      f.input :finance_account_id, :label => 'finance_account', :as => :select, :collection => FinanceAccount.all.map{|u| ["#{u.title}", u.id]}

    end
    f.actions
  end
  permit_params :name,:finance_account_id
end
