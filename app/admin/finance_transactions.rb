





ActiveAdmin.register FinanceTransaction do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    inputs "Details" do
      input :date
      input :payee
      input :description
      input :selection
      input :ref_no
      input :spent_amount
      input :received
      #input :type
      f.input :bank_id, :label => 'Bank', :as => :select, :collection => Bank.all.map{|u| ["#{u.name}", u.id]}
      f.input :customer_id, :label => 'Customer', :as => :select, :collection => Customer.all.map{|u| ["#{u.name}", u.id]}
      f.input :supplier_id, :label => 'Supplier', :as => :select, :collection => Supplier.all.map{|u| ["#{u.name}", u.id]}

    end
    f.actions
  end
  permit_params :date, :payee, :description, :type, :selection, :ref_no, :spent_amount, :received,:bank_id, :customer_id, :supplier_id
end


