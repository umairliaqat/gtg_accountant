class WelcomeController < ApplicationController
  def index
    set_meta_tags title: 'Company details',
                  site: I18n.t('views.site_name'),
                  description: 'Accounting services,Book keeping, audit services,partnership/firm/form c registraion,sole proprietorship registration,trademarks, copyrights,designs & patents,income tax,sale tax,professional tax',
                  keywords: 'Accounting services, Accountant,accounting firm,bookkeeping services,Developer,accounting and bookkeeping services,tax accounting,accounting software',
                  canonical: t('views.seo_url'),
                  index: true
    # @contact = Contact.new
  end

  def term_and_cond
  end

end
