Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html


  get '/auth/:provider/callback', to: 'oauth#callback', as: 'oauth_callback'
  get '/auth/failure', to: 'oauth#failure', as: 'oauth_failure'


  # resources :posts
  # resources :users
  # resources :linkedin do
  #   collection do
  #     get 'open_in_browser'
  #   end
  # end


  constraints(host: /^www\./i) do match '(*any)' => redirect { |params, request| URI.parse(request.url).tap { |uri| uri.host.sub!(/^www\./i, '') }.to_s }, via: [:get, :post] end

  scope "(:locale)", locale: /en|de/ do
    root 'welcome#index'
    resources :contacts
    get 'term_and_cond', to: 'welcome#term_and_cond'
  end







end
